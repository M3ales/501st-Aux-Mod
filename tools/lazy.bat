@ECHO OFF

set Arr[0]=RD501_AARF
set Arr[1]=RD501_Compositions
set Arr[2]=RD501_Droid_Dispenser
set Arr[3]=RD501_Droids
set Arr[4]=RD501_EMP
set Arr[5]=RD501_Helmets
set Arr[6]=RD501_Jumppack
set Arr[7]=RD501_Main
set Arr[8]=RD501_Markers
set Arr[9]=RD501_Particle_Effects
set Arr[10]=RD501_RDS
set Arr[11]=RD501_Units
set Arr[12]=RD501_Vehicle_Weapons
set Arr[13]=RD501_Vehicles
set Arr[14]=RD501_Weapons
set Arr[15]=RD501_Zeus
@REM set Arr[16]=VenMK2

set outputFolder=D:\mods\aux dev\Cloned Aux Dev\@501st Community Auxiliary Mod
set sourceFolder=D:\mods\aux dev\Cloned Aux Dev\addons

@REM REM Folder of the addonBuilder.exe, needed so that the script can change directory to it
set AddonBuilderPath=C:\Program Files (x86)\Steam\steamapps\common\Arma 3 Tools\AddonBuilder
set tempFolder=C:\Users\Letlev\AppData\Local\Temp

@REM REM Files to keep when building, like .hpp and shit
set includeFile=D:\mods\aux dev\Cloned Aux Dev\include.txt

@REM REM Sadly for the file below, the filepath can not have any spaces
@REM set keyFile=C:\Users\joshu\Documents\Bikeys\Howtomod.biprivatekey


CD /D %AddonBuilderPath%

set "x=0"
(
:SymLoop
if defined Arr[%x%] (
   
    call ECHO %sourceFolder%\%%Arr[%x%]%%
    call AddonBuilder.exe -clear %sourceFolder%\%%Arr[%x%]%% %outputFolder% -temp=%tempFolder% -include=%includeFile%
    set /a "x+=1"
    GOTO :SymLoop
)
) 
pause