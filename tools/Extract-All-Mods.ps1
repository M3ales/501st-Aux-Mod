$mods = @(
    "@101st Doom Battalion Auxilliary Mod",
    "@212th Auxiliary Assets",
    "@3AS (Beta Test)",
    "@ace",
    "@CBA_A3",
    "@Last Force Project",
    "@Legion Base - Stable",
    "@Just Like The Simulations - The Great War",
    "@Kobra Mod Pack - Main",
    "@Operation TREBUCHET"
)

Function Get-FolderName($initialDirectory) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $OpenFolderDialog = New-Object System.Windows.Forms.FolderBrowserDialog
    $Topmost = New-Object System.Windows.Forms.Form
    $Topmost.TopMost = $True
    $Topmost.MinimizeBox = $True
    $OpenFolderDialog.ShowDialog($Topmost) | Out-Null
    return $OpenFolderDialog.SelectedPath
}

$arma = $(Join-Path $(Get-ItemProperty 'HKCU:\SOFTWARE\Valve\Steam').SteamPath "/steamapps/common/Arma 3")
if(-Not $(Test-Path $arma)) { $arma = Get-FolderName }
if(-Not $(Test-Path $(Join-Path $arma "arma3.exe"))) {
    Write-Output "Arma not found, exiting."
    Exit 0 
}
ForEach ($mod in $mods)
{
    $path = $(Join-Path $arma "!Workshop/$mod/addons")
    & ((Get-ItemProperty HKCU:\Software\Mikero\ExtractPbo).path + "\bin\ExtractPboDos.exe") "-P" $path "P:\out"
    robocopy /e "P:\out" "P:\" /r:5 /w:5
    Remove-Item -Recurse -Force -Confirm:$false "P:\out"
}

Expand-Archive -Force "$PSScriptRoot\dummy-mods.zip" "P:\"