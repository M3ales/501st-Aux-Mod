param(
    [switch]$FullBuild = $False,
    [switch]$Pause = $False
)

$pboProject = (Get-ItemProperty "HKCU:\Software\Mikero\pboProject\").exe
$output = $(Join-Path $(Get-Location) "..\@501st Community Auxiliary Mod")
$mods = @("501st_Helmets","RD501_Compositions","RD501_Droids","RD501_Droid_Dispenser","RD501_EMP","RD501_Helmets","RD501_Jumppack","RD501_Main","RD501_Markers","RD501_Particle_Effects","RD501_RDS","RD501_Units","RD501_Vehicles","RD501_Vehicle_Weapons","RD501_Weapons","RD501_Zeus","VenMK2")
$modPath = "../addons"
$succeded = @()
$failed = @()

ForEach ($mod in $mods)
{
    $srcPath = $(Join-Path $modPath $mod)
    $target = "$output\addons\$mod.pbo"
    $LastModified = (Get-ChildItem $srcPath -File -Recurse | Sort-Object LastWriteTime | Select-Object -Last 1).LastWriteTime
    $exists = Test-Path $target
    if($exists){ $LastBuilt = (Get-ChildItem $target).LastWriteTime }
    if($exists -And -Not $FullBuild -And $LastBuilt -gt $LastModified)
    {
        Write-Verbose "$mod Up to Date, skipping."
    }
    else
    {
        Write-Output "==Building $mod=="
        $args = @()
        if(-Not $Pause){ $args += "-P" }
        $args += "-M=$output"
        $args += "P:\$mod"
        & "$pboProject" @args
        Wait-Process pboProject
        if(Test-Path $target)
        {
            Write-Output "  Successfully Built $mod."
            $succeded += $mod
        }
        else 
        {
            Write-Output "  $mod Build Failed."
            $failed += $mod
        }
    }
}
if($succeded.Count -ne 0){ Write-Output "Succeded: ", $succeded}
if($failed.Count -ne 0){ Write-Output "Failed: ", $failed}
if($succeded.Count -eq 0 -and $failed.Count -eq 0){ Write-Output "Nothing to build."}
