#include "..\RD501_Main\config_macros.hpp"

class CfgPatches
{
	class RD501_patch_Airborne_Helmet_Model
	{
		author = "";
		units[] = {};
		weapons[] = {};
		requiredAddons[]=
		{
			"RD501_patch_main"
		};
	};
};
class CfgWeapons
{
	class H_HelmetB;
	class HeadgearItem;
	class macro_new_helmet(pilot,base): H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;
		ace_hearing_protection = 0.85;
		ace_hearing_lowerVolume = 0;    
		displayName = "[501st] PLT HELM (Base)";
		model = "\501st_helmets\AB\AB_helmet_p1.p3d";
		hiddenSelections[] = {
			"camo1",
			"mat"
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Helmets\_textures\aviation\clonePilotHelmet_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "",
            "501st_Helmets\AB\data\pilot_jlts.rvmat"
        };


		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\501st_helmets\AB\AB_helmet_p1.p3d";//"SWOP_clones\helmet\CloneHelmetPilot.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {
				"camo1",
				"mat"
			};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};

		subItems[] = {"G_B_Diving","ItemcTabHCam"};
	};
};
