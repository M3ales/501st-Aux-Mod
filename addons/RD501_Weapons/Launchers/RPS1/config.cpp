#define COMPONENT RPS1
#include "../../../RD501_main/config_macros.hpp"


class CfgPatches
{
	class RD501_patch_RPS_Disposable
	{
		author=DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
		requiredAddons[]=
		{
			RD501_patch_weapons
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(launcher,rps1),
			macro_new_weapon(launcher,rps1_u)
		};
	};
};

class cfgWeapons 
{
	class Launcher_Base_F;
    class launch_RPG32_F: Launcher_Base_F {
        class WeaponSlotsInfo;
    };

	class macro_new_weapon(launcher,rps1) : launch_RPG32_F
	{
		author= "RD501";
		scope = 2;
		scopeArsenal=2;
		displayName = "Republic RPS1 Launcher";
		model = "\3AS\3AS_Weapons\RPS6\3AS_RPS6_F.p3d";
        icon = "\RD501_Weapons\Launchers\RPS1\rps_disposable_icon.paa";
		picture = "\RD501_Weapons\Launchers\RPS1\rps_disposable_icon.paa";
		recoil = "recoil_single_law";
		baseWeapon = MACRO_QUOTE(macro_new_weapon(launcher,rps1));
		RD501_Empty_Weapon = MACRO_QUOTE(macro_new_weapon(launcher,rps1_u));
		magazines[] = { macro_new_mag(rps1,1) };
		magazineWell[]={""};
		magazineReloadTime = 0.1;
        reloadMagazineSound[] = {"",1,1};
		class EventHandlers {
			fired = MACRO_QUOTE(_this call macro_fnc_name(onWeaponFiredSwapToEmpty));
		};
        class WeaponSlotsInfo: WeaponSlotsInfo {
            mass = 80;
        };
	};
class macro_new_weapon(launcher,rps1_u) : macro_new_weapon(launcher,rps1)
	{
		author= "RD501";
		scope = 1;
		scopeArsenal=1;
		displayName = "Republic RPS1 Launcher (Used)";
		icon = "\RD501_Weapons\Launchers\RPS1\rps_disposable_icon.paa";
		picture = "\RD501_Weapons\Launchers\RPS1\rps_disposable_icon.paa";
		baseWeapon = MACRO_QUOTE(macro_new_weapon(launcher,rps1_u));
		magazineWell[]={"empty"};
		magazines[] = { "empty" };
        class WeaponSlotsInfo: WeaponSlotsInfo {
            mass = 10;
        };
	};
};