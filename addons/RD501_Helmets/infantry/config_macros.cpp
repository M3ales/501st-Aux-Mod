//Normal Infantry ones
	#define NEW_501_Inf_Helm(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,base)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##texture};\
		};

//ARC
	#define NEW_501_ARC_Helm(classname,displayname,texture)\
		class macro_new_helmet(arc,classname) : macro_new_helmet(arc,base_jlts)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\arc\##texture};\
		};	

//Lumanated Visor
	#define NEW_501_Lumanated_Visor_Helm(classname,displayname,texture)\
		class macro_new_helmet(infantry_lumanated,classname) : macro_new_helmet(infantry,lum_base)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##texture};\
		};
		
//Normal Infantry ones JLTS
	#define NEW_501_Inf_Helm_JLTS(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,jlts_recruit)\
		{\
			scope = 2;\
			displayName = [501st] INF HELM ('##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##texture};\
		};

//ARC JLTS
	#define NEW_501_ARC_Helm_JLTS(classname,displayname,texture)\
		class macro_new_helmet(arc,classname) : macro_new_helmet(arc,base_jlts)\
		{\
			scope = 2;\
			displayName = [501st] ARC HELM ('##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\arc\##texture};\
		};

//BARC JLTS
	#define NEW_501_BARC_Helm_JLTS(classname,displayname,texture)\
		class macro_new_helmet(arc,classname) : macro_new_helmet(barc,base_jlts)\
		{\
			scope = 2;\
			displayName = [501st] INF MED HELM 06 (##displayname##);\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\jlts\inf\##texture};\
		};

//Mynock 3AS
	#define NEW_501_MYN_Helm_JLTS(classname,displayname,texture)\
		class macro_new_helmet(mynock,classname) : macro_new_helmet(mynock,base_3as)\
		{\
			scope = 2;\
			displayName = [501st] MYN HELM ('##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\Mynock\##texture};\
		};

//Lumanated Visor JLTS
	#define NEW_501_Lumanated_Visor_Helm_JLTS(classname,displayname,texture)\
		class macro_new_helmet(infantry_lumanated,classname) : macro_new_helmet(infantry,jlts_lum_base)\
		{\
			scope = 2;\
			displayName = [501st] INF HELM ('##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\infantry\##texture};\
		};
//LS shit
	#define NEW_501_Inf_Helm_LS(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,ls_base)\
		{\
			scope = 2;\
			displayName = [501st] INF RTO HELM ('##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\LS\##texture};\
		};
//LF helms
	#define NEW_501_Inf_Helm_LF(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,lf_base)\
		{\
			scope = 2;\
			displayName = [501st] INF SNOW HELM (##displayname##);\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\LF\##texture};\
		};
//3AS helms
	#define NEW_501_Inf_Helm_3AS(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,3AS_base)\
		{\
			scope = 2;\
			displayName = Clonetrooper helmet (501st '##displayname##');\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\3AS\##texture};\
		};
//JLTSv2
	#define NEW_501_Inf_Helm_JLTS2(classname,displayname,texture)\
		class macro_new_helmet(infantry,classname) : macro_new_helmet(infantry,jlts_recruit)\
		{\
			scope = 2;\
			displayName = [501st] INF MED HELM 03 (##displayname##);\
			hiddenSelectionsTextures[] = {macro_custom_helmet_textures##\jlts\inf\##texture};\
		};