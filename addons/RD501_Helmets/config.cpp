
#include "../RD501_main/config_macros.hpp"
#define TEXTUREAB \RD501_Helmets\_textures\airborne
class CfgPatches
{
	class RD501_patch_helmets
	{
		author=DANKAUTHORS;
		requiredAddons[]=
		{
			macro_lvl1_req
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_helmet(infantry,jlts_recruit)
		};
	};
};

class CfgWeapons
{
	class HeadgearItem;
	class H_HelmetB;
    class SWLB_P2_SpecOps_Helmet;
    class 21st_clone_P2_helmet;
    class 3as_P1_Base;
	
	class macro_new_helmet(empire_rg,boi) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    

		displayName = "[Empire] Royal Guard Helm 01";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetSCC.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"RD501_Units\textures\gcw\empire\RG\rg_helmet.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetSC.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
    //jlts
	class macro_new_helmet(arc,base_jlts) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] ARC HELM (Base)";
		picture="\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetARC_ui_ca.paa";
		model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\ARC\ARC.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
			hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat"};
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
    class macro_new_helmet(barc,base_jlts) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF MED HELM 06 (CM-C)";
		picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetBARC_ui_ca.paa";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cm_c_barc_helmet.paa"};
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
        hiddenSelectionsMaterials[]= {"mrc\jlts\characters\clonearmor2\data\clone_helmet_barc.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
			hiddenSelectionsMaterials[]= {"mrc\jlts\characters\clonearmor2\data\clone_helmet_barc.rvmat"};
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
	class macro_new_helmet(arc,sgt_jlts) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] ARC HELM (Sergeant)";
		picture="\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetARC_ui_ca.paa";
		model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\ARC\ARC_CS.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
			hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat"};
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};

	//infantry
	class macro_new_helmet(infantry,jlts_recruit) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF HELM 01 (Base)";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\recruit_helmet.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
	class macro_new_helmet(infantry,jlts_cadet) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF HELM 02 (Cadet)";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cadet_helmet.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
	class macro_new_helmet(infantry,jlts_trooper) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   

		displayName = "[501st] INF HELM 03 (Trooper)";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\trooper_helmet.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
	class macro_new_helmet(infantry,jlts_sgt) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   

		displayName = "[501st] INF HELM 04 (Sgt)";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\sgt_helmet.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};	
	class macro_new_helmet(infantry,jlts_odin) : H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		RD501_isNV = 1;
		displayName = "[501st] INF HELM ('Odin')";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Infantry\Odin.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};		
	class macro_new_helmet(airborne,jlts_trooper): H_HelmetB
	{
		author = "RD501";
		scope = 2;
		scopeArsenal = 2;
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] AB HELM 02 (Trooper)";
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\ab\ab_helmet_trooper.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			hiddenSelections[] = {"camo1"};
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 60;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(airborne,jlts_vtrooper): H_HelmetB
	{
		author = "RD501";
		scope = 2;
		scopeArsenal = 2;
		displayName = "[501st] AB HELM 03 (Vet. Trooper)";
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\ab\ab_helmet_vtrooper.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			hiddenSelections[] = {"camo1"};
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 60;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(airborne,jlts_base): H_HelmetB
	{
		author = "RD501";
		scope = 2;
		scopeArsenal = 2;
		displayName = "[501st] AB HELM 01 (Base)";
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
		picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_Helmet_AB_co.paa"};
		hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			hiddenSelections[] = {"camo1"};
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 60;
					passThrough = 0.5;
				};
			};
		};
	};
    //legion
    class macro_new_helmet(infantry,ls_base) : SWLB_P2_SpecOps_Helmet
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF RTO HELM 01 (Alt)";
		model = "SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		hiddenSelections[] = {"Camo1"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LS\rto_alt_helmet.paa"};
		hiddenSelectionsMaterials[]= {"swlb_cee\data\SWLB_P2_SpecOps.rvmat"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
			modelSides[] = {6};
			hiddenSelections[] = {"Camo1"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
    //lost force/last force
    class macro_new_helmet(infantry,lf_base) : 21st_clone_P2_helmet
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF SNOW HELM 01 (Trooper)";
        hiddenSelections[] = {"Helmet"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\snow_trooper_helmet.paa"};
        hiddenSelectionsMaterials[]= {"armor_unit\21\helmet\helm.rvmat"};
		model = "\armor_unit\21\HelmetP2_21.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\armor_unit\21\HelmetP2_21.p3d";
			picture = "armor_unit\21\ui\21_Armor.paa";
			modelSides[] = {6};
			hiddenSelections[] = {"Helmet"};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.6;
				};
			};
		};
	};
	class macro_new_helmet(infantry,snow_nco) : macro_new_helmet(infantry,lf_base)
	{
		displayName = "[501st] INF SNOW HELM 02 (NCO)";
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\snow_nco_helmet.paa"};
	};
    //3as
    class macro_new_helmet(infantry,p1_trooper): H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF P1 HELM 01 (Trooper)";
		picture = "3AS\3AS_Characters\Clones\Headgear\ui\phase1_ui_ca.paa";
		model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Unmarked_CO.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
			hiddenSelections[] = {"camo"};
			modelSides[] = {3,1};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(infantry,p1_trooper_nco): macro_new_helmet(infantry,p1_trooper)
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] INF P1 HELM 02 (NCO)";
		picture = "3AS\3AS_Characters\Clones\Headgear\ui\phase1_ui_ca.paa";
		model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\3AS\p1_nco_helmet.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
			hiddenSelections[] = {"camo"};
			modelSides[] = {3,1};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(arc,p1_trooper): H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] ARC P1 HELM 01 (Trooper)";
		picture = "3AS\3AS_Characters\Clones\Headgear\ui\phase1_ui_ca.paa";
		model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Lieutenant_CO.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
			hiddenSelections[] = {"camo"};
			hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Lieutenant_CO.paa"};
			modelSides[] = {3,1};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(arc,p1_nco): H_HelmetB
	{
		scope = 2;
		scopeArsenal = 2;
		author = "RD501";
		weaponPoolAvailable = 1;       
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;   
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		displayName = "[501st] ARC P1 HELM 02 (NCO)";
		picture = "3AS\3AS_Characters\Clones\Headgear\ui\phase1_ui_ca.paa";
		model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Captain_CO.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 30;
			uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
			hiddenSelections[] = {"camo"};
			hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Captain_CO.paa"};
			modelSides[] = {3,1};
			material = -1;
			explosionShielding = 2.2;
			minimalHit = 0.01;
			passThrough = 0.01;
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 50;
					passThrough = 0.5;
				};
			};
		};
	};
	class macro_new_helmet(mynock,3as_base): H_HelmetB
	{
		author = "RD501";
		scope = 2;
		scopeArsenal = 2;
		displayName = "[501st] MYN HELM 01 (Base)";
		subItems[] = {"G_B_Diving","ItemcTabHCam"};
		model="\3AS\3AS_Characters\Clones\Headgear\3AS_P2_Clone_Helm_Tanker.p3d";
		picture="3AS\3AS_Characters\Clones\Headgear\ui\phase1_ui_ca.paa";
		ace_hearing_protection = 0.85; 		
		ace_hearing_lowerVolume = 0;    
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Tanker\501st_co.paa"};
		class ItemInfo: HeadgearItem
		{
			mass = 10;
			uniformmodel="\3AS\3AS_Characters\Clones\Headgear\3AS_P2_Clone_Helm_Tanker.p3d";
			allowedSlots[] = {801,901,701,605};
			modelSides[] = {6};
			hiddenSelections[] = {"camo"};
			class HitpointsProtectionInfo
			{
				class Head
				{
					hitpointName = "HitHead";
					armor = 60;
					passThrough = 0.5;
				};
			};
		};
	};
};
