#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _version = getArray(configFile >> "CfgPatches" >> ADDON >> "version");
LOG(format["Version: %1", _verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

// Vehicle MFD/Colour changes
[QGVAR(custom), "COLOR", ["Custom Hud Colour", "Its a colour, pick."], ["RD501 Misc", "Custom Colour"], [0.5,0.5,0.5], 2, {}, false] call CBA_fnc_addSetting;
[QGVAR(custom_alpha), "SLIDER", ["Custom Hud Alpha", "Its transparency, pick."], ["RD501 Misc", "Custom Colour"], [0, 1, 1, 4], 2, {}, false] call CBA_fnc_addSetting;

LOG("PreInit Complete");