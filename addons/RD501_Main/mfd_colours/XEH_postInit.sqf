#include "functions\function_macros.hpp"

private _propertyName = QGVAR(enabled);
private _enabledSearchCondition = format["getNumber (_x >> '%1') > 0", _propertyName];
GVAR(validTypes) = _enabledSearchCondition configClasses (configFile >> "CfgVehicles");

["ace_interact_menu_newControllableObject", {
    params ["_type"]; // string of the object's className
	private _validTypes = GVAR(validTypes);
    if (_validTypes findIf { _type isKindOf (_x) } == -1) exitWith {};
	[_type] call FUNC(addActionsToClass);
}] call CBA_fnc_addEventHandler;
