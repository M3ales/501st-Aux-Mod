//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon light_infantry_transport
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_lit_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_light_infantry_transport
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_vehicles)
		};
		requiredVersion=0.1;
		units[]=
		{
			MACRO_QUOTE(macro_new_vehicle(light_infantry_transport,CIS_MkII))
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers ;

class CfgVehicles
{

	#include "inheritance.hpp"
	class 3as_SAC_Trade;
	class macro_new_vehicle(light_infantry_transport,CIS_MkII):3as_SAC_Trade
	{
		
		scope = 2;
		scopeCurator = 2;
		forceInGarage = 1;
		displayName = "CIS Tempest";
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		faction = MACRO_QUOTE(macro_faction(CIS));
        editorSubcategory = MACRO_QUOTE(macro_editor_cat(car));
        vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(Car));

		class EventHandlers
		{
			init = "[_this select 0] execVM 'RD501_Vehicles\_init_functions\wheel_dmg.sqf';";
		};
	};	
};
