//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon LAAT
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_laat(name) CONCAT_3(vehicle_classname,_,name)

class CfgPatches
{
	class RD501_patch_laat
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles,
			"3AS_LAAT",
			"3AS_LAAT_Cargo"
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_laat(Mk1),
			macro_new_laat(Mk1_lights),
			macro_new_laat(Mk2),
			macro_new_laat(Mk2_lights),
			macro_new_laat(LE),
			macro_new_laat(C)
		};
		weapons[]=
		{
			
		};
	};
};
#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers; 
class CfgVehicles
{

	#include "inheritance.hpp"

	class macro_new_laat(Mk1):3as_LAAT_Mk1
	{
		displayName="LAAT/I MK.I";

		#include "common_stuff_tcw.hpp"
		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};

		class UserActions: UserActions
		{
			#include "user_action.hpp"
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\LAAT\hull_tmp_co.paa",
            "3AS\3as_Laat\LAATI\data\wings_501_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapons_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapon_Details_CO.paa",
			"3AS\3as_Laat\LAATI\data\interior_CO.paa"
        };

		class Turrets: Turrets
		{
			class Gunner: Copilot
			{
				minelev=-60;
				minturn=-240;
				maxelev=40;
				maxturn=-120;
				#include "../../common/common_optics.hpp"
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				memorypointgunneroptics="FIXME";
			};
			class LeftDoorgun: LeftDoorgun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_l),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300)
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
			};
			class RightDoorGun: RightDoorGun
			{
				weapons[]=
				{
					macro_new_weapon(turret,laat_ball_beam_r),
					"Laserdesignator_pilotCamera"
				};
				magazines[]=
				{
					"Laserbatteries",
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),
					macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300),macro_new_mag(laat_ball_beam,300)
				};
				#include "../../common/common_optics.hpp"
				memorypointgunneroptics="FIXME";
			};
			class CargoTurret_01: CargoTurret_01{};
			class CargoTurret_02: CargoTurret_02{};
			class CargoTurret_03: CargoTurret_03{};
			class CargoTurret_04: CargoTurret_04{};
			class CargoTurret_05: CargoTurret_05{};
			class CargoTurret_06: CargoTurret_06{};
		};
	};
	class macro_new_laat(Mk1_lights):3as_LAAT_Mk1Lights
	{
		displayName="LAAT/I MK.I (Lights)";

		#include "common_stuff_tcw.hpp"

		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};

		class UserActions: UserActions
		{
			#include "user_action.hpp"
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\LAAT\hull_tmp_co.paa",
            "3AS\3as_Laat\LAATI\data\wings_501_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapons_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapon_Details_CO.paa",
			"3AS\3as_Laat\LAATI\data\interior_CO.paa"
        };

		class Turrets: Turrets
		{
			class Gunner: Copilot
			{
				minelev=-60;
				minturn=-240;
				maxelev=40;
				maxturn=-120;
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				#include "../../common/common_optics.hpp"
			};
			class CargoTurret_01: CargoTurret_01{};
			class CargoTurret_02: CargoTurret_02{};
			class CargoTurret_03: CargoTurret_03{};
			class CargoTurret_04: CargoTurret_04{};
			class CargoTurret_05: CargoTurret_05{};
			class CargoTurret_06: CargoTurret_06{};
		};
	};

	class macro_new_laat(Mk2):3as_LAAT_Mk2
	{
		displayName="LAAT/I MK.II";

		#include "common_stuff_tcw.hpp"

		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};

		class UserActions: UserActions
		{
		#include "user_action.hpp"
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\LAAT\hull_tmp_co.paa",
            "3AS\3as_Laat\LAATI\data\wings_501_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapons_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapon_Details_CO.paa",
			"3AS\3as_Laat\LAATI\data\interior_CO.paa"
        };

		class Turrets: Turrets
		{
			class Gunner: Copilot
			{
				minelev=-60;
				minturn=-240;
				maxelev=40;
				maxturn=-120;
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				#include "../../common/common_optics.hpp"
			};
			class CargoTurret_01: CargoTurret_01{};
			class CargoTurret_02: CargoTurret_02{};
		};
	};
	class macro_new_laat(Mk2_lights):3as_LAAT_Mk2Lights
	{
		displayName="LAAT/I MK.II (Lights)";

		#include "common_stuff_tcw.hpp"

		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};

		class UserActions: UserActions
		{
			#include "user_action.hpp"
		};

		hiddenSelectionsTextures[]=
        {
            "RD501_Vehicles\textures\LAAT\hull_tmp_co.paa",
            "3AS\3as_Laat\LAATI\data\wings_501_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapons_CO.paa",
			"3AS\3as_Laat\LAATI\data\weapon_Details_CO.paa",
			"3AS\3as_Laat\LAATI\data\interior_CO.paa"
        };

		class Turrets: Turrets
		{
			class Gunner: Copilot
			{
				minelev=-60;
				minturn=-240;
				maxelev=40;
				maxturn=-120;
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000),
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				#include "../../common/common_optics.hpp"
			};
			class CargoTurret_01: CargoTurret_01{};
			class CargoTurret_02: CargoTurret_02{};
		};
	};
	class macro_new_laat(LE):3AS_Patrol_LAAT_Republic
	{
		displayName="Republic LAAT/LE";
		scope=2;
		author="RD501";
		forceInGarage = 1;

		RD501_magclamp_large_offset[] = {0.0, 0.0, -4.5};
		RD501_magclamp_small_offset[] = {0.0, 0.0, -4.5};

		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_heli));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		transportSoldier=14;
		class UserActions
		{
			class ThrusterEngage
			{
				displayName = "";
				displayNameDefault = "";
				textToolTip = "";
				position = "pilotview";
				radius = 20;
				priority = 0;
				onlyForPlayer = 1;
				condition = "((player == driver this) AND (alive this))";
				statement = "this execVM ""\RD501_Main\functions\impulse\fnc_impulseIncrease_LE.sqf""";
				shortcut="User19";
			};

			class ThrusterDisengage: ThrusterEngage
			{
				priority = 0;
				displayName = "";
				displayNameDefault = "";
				textToolTip = "";
				condition = "((player == driver this) AND (alive this))";
				statement = "this execVM ""\RD501_Main\functions\impulse\fnc_impulseDecrease_LE.sqf""";
				shortcut="User20";
			};
		};

		RD501_magclamp_small_1[] = {0.0,1.0,-1.0};
		enableManualFire=1;
		weapons[]=
			{
				macro_new_weapon(generic,republic_aircraft_cannon),
				"Bomb_Leaflets",
				macro_new_weapon(wynd,ugm),
				macro_basic_air_weapons
				
			};
		magazines[]=
			{
				macro_basic_air_mags,
				"1Rnd_Leaflets_Civ_F",
				"1Rnd_Leaflets_Civ_F",
				"1Rnd_Leaflets_Civ_F",
				"1Rnd_Leaflets_Civ_F",
				macro_new_mag(ugm,10),
				macro_new_mag(generic_aircraft_gun_green,1000)
			};
		class Turrets: Turrets
		{

			class Gunner: Copilot
			{
				outGunnerMayFire=1;
				commanding=-1;
				primaryGunner=1;
				weapons[]=
					{
						macro_new_weapon(generic,republic_aircraft_cannon),
						"Laserdesignator_pilotCamera"
					};
				magazines[]=
					{
						"Laserbatteries",
						macro_new_mag(generic_aircraft_gun_green,1000)
					};
				#include "../../common/common_optics.hpp"
			};
		};
	};
	class macro_new_laat(C):3AS_LAATC
	{
		displayName="Republic LAAT/C";
		scope=2;
		author="RD501";
		forceInGarage = 1;

		hiddenSelections[] = {"camo","camo1"};
		hiddenSelectionsTextures[] = {"RD501_Vehicles\textures\LAAT\Camo_0_CO.paa","RD501_Vehicles\textures\LAAT\Camo_1_CO.paa"};

		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_heli));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		class UserActions
		{
			class ThrusterEngage
            {
                displayName = "";
                displayNameDefault = "";
                textToolTip = "";
                position = "pilotview";
                radius = 20;
                priority = 0;
                onlyForPlayer = 1;
                condition = "((player == driver this) AND (alive this))";
                statement = "this execVM ""\RD501_Main\functions\impulse\fnc_impulseIncrease.sqf""";
                shortcut="User19";
            };

            class ThrusterDisengage: ThrusterEngage
            {
                priority = 0;
                displayName = "";
                displayNameDefault = "";
                textToolTip = "";
                condition = "((player == driver this) AND (alive this))";
                statement = "this execVM ""\RD501_Main\functions\impulse\fnc_impulseDecrease.sqf""";
                shortcut="User20";
            };
			class afterburnerMk1_turn_on
			{
				condition="false;";
			};
			class afterburnerMk1_turn_off
			{
				condition="false;";
			};
		};
		
		RD501_magclamp_small_1[] = {-7.0,-2.0,-4.5};
		RD501_magclamp_large[] = {0.0,-2.0,-4.0};
		RD501_magclamp_small_2[] = {7.0,-2.0,-4.5};
		RD501_magclamp_small_forbidden = 1;
		RD501_magclamp_large_offset[] = {0.0,1.0,-4.5};
		
		enableManualFire=1;
		weapons[]=
		{
			macro_new_weapon(generic,republic_aircraft_cannon),
			macro_new_weapon(cannon,laat),
			macro_basic_air_weapons
		};
		magazines[]=
		{
			macro_basic_air_mags,
			macro_new_mag(generic_aircraft_gun_green,1000),
			macro_new_mag(laat_cannon,100),
		};
	};
};